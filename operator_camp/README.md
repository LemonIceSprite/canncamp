# Operator Campus

### 介绍
昇腾CANN训练营——算子开发营。算子开发营一共有三次课程，每节课程结束时会发布本节课的作业，大家根据发布的作业内容完成对应的作业。
- 如果涉及代码
    
    首先，将[canncamp仓](https://gitee.com/ascend/canncamp)fork到个人分支，基于个人分支在`operator_camp`文件夹下新增自己的工程文件夹。文件夹命名规则为`task_x_华为云账号`；比如：`task_1_lin6413`。

    其次，上传源码。将自己的源码添加到新建的文件夹中，删除冗余的代码/文件，完善readme，并更新到自己的个人分支。

    最后，提交PR。在提交PR时，算子营的标题请标注为【昇腾CANN训练营——算子训练营】第x周 课程名称 华为云账号。比如：
    ```
    【昇腾CANN训练营——算子训练营】第4周 XXX算子 fulltower
    ```
    
- 如果是客观题。请移步到[昇腾论坛](https://bbs.huaweicloud.com/forum/thread-113294-1-1.html)，按要求提交。

### 课程作业

- [算子营第2周作业](https://gitee.com/ascend/canncamp/blob/master/operator_camp/1.md)

- [算子营第3周作业](https://gitee.com/ascend/canncamp/blob/master/operator_camp/2.md)

- [算子营第4周作业](https://gitee.com/ascend/canncamp/blob/master/operator_camp/3.md)
